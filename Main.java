public class Main {
    public static void main(String[] args) {

        BaseElement base = new BaseElement();
        LinkElement link = new LinkElement();
        TextElement text = new TextElement();

        base.getLocator();
        base.click();

        link.getElement();
        link.click();

        text.getElement();
        text.click();

    }
}
